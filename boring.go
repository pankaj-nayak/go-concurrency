package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Message struct {
	str  string
	wait chan bool
}

func main() {

	// boring function returns a channel on which they are continously sending
	// values and in main we are going to see what they are doing by reading on
	// that value. After sometime we realize both of them are boring and we
	// return from there

	//	manish := boringFunction("Manish")
	//	janish := boringFunction("Janish")

	c := fanIn(boringFunction("Niraj"), boringFunction("diksha"))
	for i := 0; i < 10; i++ {
		fmt.Println("Main: reading value =", <-c)
		//fmt.Println("Main: reading value =", <-janish)
	}

	time.Sleep(time.Millisecond)
	fmt.Println("Main : You're both boring; I'm leaving.")

}

func boringFunction(msg string) <-chan string {
	out := make(chan string)

	go func() {
		for i := 0; ; i++ {
			out <- fmt.Sprintf(" %q %v ", msg, i)
		}
	}()
	time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
	return out
}

func fanIn(input1, input2 <-chan string) <-chan string {

	out := make(chan string)

	go func() {
		for {
			out <- <-input1
		}
	}()
	go func() {
		for {
			out <- <-input2
		}
	}()
	return out
}
