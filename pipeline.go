package main

import "fmt"

func gen(nums ...int) <-chan int {

	out := make(chan int)

	go func() {

		for _, n := range nums {

			out <- n

		}
	}()

	return out
}

func square(in <-chan int) <-chan int {

	out := make(chan int)

	go func() {

		for n := range in {

			out <- n * n
		}

	}()
	return out

}

func main() {
	fmt.Println(" GO Concurrency Check...")

	input := []int{2, 3, 4}

	c := gen(input...)

	s := square(c)

	for n := range s {

		fmt.Println("value are : ", n)
	}
}
